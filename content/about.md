---
slug: about
title: About me
---


<img src="../images/Darya1.jpg" height="200" style="padding: 30 10px; float: right;">

Hi! I'm Darya, a Data Scientist & Data Analytics trainer with a background in genomics and bioinformatics. I work at the University of Sydney's [Informatics Hub](https://informatics.sydney.edu.au), where I develop advanced analytics training for research staff at the university and lend my data science and coding snafu to projects with university partners. 

- I hold a PhD in Genomics and Bioinformatics from the University of Queensland, where I investigated novel ncRNA transcription in human and mouse stem cell and nervous system development. I've also done a PostDoc at the Centenary Institute, investigating alternative splicing dynamics and regulation. You can have a look at my papers [here](https://www.ncbi.nlm.nih.gov/pubmed/?term=Vanichkina+D%5BAuthor%5D).


- I’m proud to be a Software and Data Carpentry Instructor, Maintainer, Curriculum Contributor, and Mentor, and am passionate about enabling reproducible research using open source tools.

