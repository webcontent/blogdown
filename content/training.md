---
slug: training
title: Training
---

I develop and deliver training for researchers at the University of Sydney. Please see [here](http://informatics.sydney.edu.au/training) for upcoming courses.

Recently, I:

- Developed and delivered a 3/4 day [RNA-seq data analysis course using R + HPC](https://sydney-informatics-hub.github.io/training-RNAseq/). [Slides](https://sydney-informatics-hub.github.io/training-RNAseq-slides/01_IntroductionToRNASeq/01_IntroductionToRNASeq.html#1).
- Co-developed and delivered a 2 day [Machine learning using R course (Sydney Uni access only)](https://pages.github.sydney.edu.au/informatics/lessons-mlr/).
- Co-organised, co-developed python and machine learning content for and co-delivered a 5 day [Business School Data Science Summer School (Sydney Uni access only)](https://pages.github.sydney.edu.au/informatics/2019_BSDSSS/), including running a data hackathon for the attendees.
- Adapted and delivered the [Data Carpentry R for Geospatial Data Analysis](https://sydney-informatics-hub.github.io/2018_12_10_GISworkshop/) at the University of Sydney.
- Co-developed and delivered an [Introductory R for Biomedical researchers](https://sydney-informatics-hub.github.io/lessonbmc/) course at the University of Sydney's Brain and Mind Centre.

I'm also currently studying in the Graduate Certificate of Higher Education Programme at the University of Sydney, to improve my training delivery and practice.

## Teaching philosophy

I am passionate about teaching and training, especially programming, analytics and reproducible research foundations to researchers. I believe that all of us who attempt to systematically learn more about the world need to be equipped with the tools - which only large-scale projects, programming and data can provide - to look in an unbiased and reproducible manner. I am a proponent of active learning, and (when appropriate!) project-based and activity-based learning. I believe that the combination of innovative strategies in teaching, including peer learning and flipped classrooms, with the best that the digital revolution has to offer (video recordings of lectures, podcasts, screencasts, interactive web-based learning environments and MOOCs from the world’s best thinkers) makes today an especially fortunate time to be a learner. Students need no longer be limited by where they are geographically to learn, and instead of working on mundane fill-in-the-blanks secret class assignments can set up web resources, blogs and apps to showcase skills they’ve acquired to future employers as part of a truly modern education in the XXI century.


I believe that because research around the world is funded largely by taxpayer investment, we as a community should be held accountable to those outside of our profession - which is why I am involved in outreach activities to discuss the work that I do with the general public.

Finally, I believe in openness: open source, open access and open data - because all of us should be able to reap the rewards of living in the technological marvel that is the XXI century.

