---
slug: contact
title: Contact
---

#### Physical address

    Sydney Informatics Hub
    32 Queen Street
    Ground Floor
    Camperdown NSW 2008
    T +61 4 2088 9939 | Skype: darya_van
#### Electronic address
d.[lastname] at gmail [dot] com
#### Links to other websites
* <a href="https://github.com/dvanic">GitHub</a>
* <a href="http://uq.academia.edu/DaryaVanichkina">Academia.edu</a>
* <a href="http://www.epernicus.com/dpv">Epernicus</a>
* <a href="http://figshare.com/authors/Darya_Vanichkina/278260">Figshare</a>
* <a href="http://orcid.org/0000-0002-0406-164X"> ORCID </a>
* <a href="https://www.researchgate.net/profile/Darya_Vanichkina/">ResearchGate</a>
* <a href="http://www.slideshare.net/DaryaVanichkina1"> Slideshare </a>
* <a href="https://scholar.google.com/citations?user=rTMDV6UAAAAJ"> Google Scholar</a>